from imutils import face_utils
import numpy as np
import math
import dlib
import cv2
import sys

# http://sourceforge.net/projects/dclib/files/dlib/v18.10/shape_predictor_68_face_landmarks.dat.bz2
p = "shape_predictor_68_face_landmarks.dat"
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(p)

cap = cv2.VideoCapture(0)
shape1 = None

while True:
	_, image = cap.read()
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	rects = detector(gray, 0)
	for (i, rect) in enumerate(rects):
		shape = face_utils.shape_to_np(predictor(gray, rect))
		x_diff = shape[45][0] - shape[36][0]
		y_diff = shape[45][1] - shape[36][1]
		angle = round(math.degrees(math.atan2(y_diff, x_diff))/60,2)
		print ("RL=", angle,sep="")
		cv2.putText(image, "lateral " + str(angle),(10,100), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (255,255,0), 1, cv2.LINE_AA)

		nariz =  np.asarray( shape[27] - shape[33])
		ojos =  np.asarray( shape[27] - shape[38])
		angle =  round(((90/np.linalg.norm(nariz)*np.linalg.norm(ojos)) - 45) /30, 2)
		print ("RH=", angle,sep="")
		cv2.putText(image, "horizontal " + str(angle),(10,120), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0,255,0), 1, cv2.LINE_AA)

		x_diff = (shape[16][0])- shape[27][0]
		y_diff = (shape[16][1]) - shape[27][1]
		angle =  round(math.degrees(math.atan2(y_diff, x_diff))/30, 2)
		print ("RV=", angle,sep="")
		cv2.putText(image, "vertical " + str(angle),(10,140), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (200,0,255), 1, cv2.LINE_AA)

		frenillo =  np.asarray( shape[33] - shape[51])
		boca =  np.asarray( shape[57] - shape[51])
		angle =  np.linalg.norm(boca) > (np.linalg.norm(nariz)*.65)
		print ("BOCA=", angle,sep="")
		cv2.putText(image, "boca " + str(angle),(10,160), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0,255,255), 1, cv2.LINE_AA)

		ojoI =  np.asarray( shape[40] - shape[38])
		ojoD =  np.asarray( shape[43] - shape[47])
		D = np.linalg.norm(ojoI) > (np.linalg.norm(nariz)/10)
		I = np.linalg.norm(ojoD) > (np.linalg.norm(nariz)/10)

		print ("OJOI=", int(I),sep="")
		print ("OJOD=", int(D),sep="")

		cv2.putText(image, "ojo I " + str(I),(10,170), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0,255,255), 1, cv2.LINE_AA)
		cv2.putText(image, "ojo D " +str(D),(10,180), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0,255,255), 1, cv2.LINE_AA)
		sys.stdout.flush()

		for (x, y) in shape:
			cv2.circle(image, (x, y), 3, (0, 0, 255), -1)

	cv2.imshow("Output", image)
	k = cv2.waitKey(5) & 0xFF
	if k == 27:
		break	

cv2.destroyAllWindows()
cap.release()